# Exercise brief

Design an API for a vending machine, allowing users with a “seller” role to add, update or remove products, while users with a “buyer” role can deposit coins into the machine and make purchases. Your vending machine should only accept 5, 10, 20, 50 and 100 cent coins

**Tasks**

- REST API should be implemented consuming and producing “application/json”
- Implement product model with amountAvailable, cost (should be in multiples of 5), productName and sellerId fields
- Implement user model with username, password, deposit and role fields
- Implement an authentication method (basic, oAuth, JWT or something else, the choice is yours)
- All of the endpoints should be authenticated unless stated otherwise
- Implement CRUD for users (POST /user should not require authentication to allow new user registration)
- Implement CRUD for a product model (GET can be called by anyone, while POST, PUT and DELETE can be called only by the seller user who created the product)
- Implement /deposit endpoint so users with a “buyer” role can deposit only 5, 10, 20, 50 and 100 cent coins into their vending machine account (one coin at the time)
- Implement /buy endpoint (accepts productId, amount of products) so users with a “buyer” role can buy a product (shouldn't be able to buy multiple different products at the same time) with the money they’ve deposited. API should return total they’ve spent, the product they’ve purchased and their change if there’s any (in an array of 5, 10, 20, 50 and 100 cent coins)
- Implement /reset endpoint so users with a “buyer” role can reset their deposit back to 0
- Think about possible edge cases and access issues that should be solved

# About solution

Vending machine backend.

Technical stack:
- FastAPI (uvicorn, SQLAlchemy, Pydantic, Swagger)
- DataBase SQLite

DataBase is created on the start if it doesn't exist. The same DB is used for development and testing.

Once server started the documentation is available on the page http://localhost:8000/docs/

Two users are created if they don't exists in DB:
1. Login: John, password: John, role: buyer
1. Login: Olivier, password: Olivier, role: seller

Tokens JWT are used for the authorizations.

File .env was added to git repository intentionally. For security reasons it should not be in git repository for real applications.

# Getting started

### Create a virtual environment Python
```
python3 -m venv env
```

### Activate the virtual environment
#### Windows
```
.\env\Scripts\activate
```
#### Linux
```
source env/bin/activate
```

### Install requirements
```
pip install -r requirements.txt
```

### Start uvicorn server
```
python3 -m uvicorn app.main:app --reload --proxy-headers --host 0.0.0.0 --port 8000
```

# Testing
Execute the command
```
python -m pytest app/tests/
```
